/*
 * ------------------------------------------------------------------
 * Copyright © 2018 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product:
 *   Module Name:
 *  Date Created: 2021-03-03
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * 2021-03-03     x0334
 * ------------------------------------------------------------------
 */

package com.example.demo001;

class UnifyConstantUtil {
    public static final String UNIT_FORMAT = "%.2f";
    public static final String KB = "KB";
    public static final String MB = "MB";
    public static final String GB = "GB";
    public static final String PB = "PB";
}


public class TestDouble {
    static String adjustUnit(double size) {

        String sizeStr;
        // 单位转换
        if (size >= 0 && size < 1048576) {
            sizeStr = String.format(UnifyConstantUtil.UNIT_FORMAT,
                    size / 1024) + UnifyConstantUtil.KB;
        } else if (size >= 1048576 && size < 1073741824) {
            sizeStr = String.format(UnifyConstantUtil.UNIT_FORMAT,
                    size / 1024 / 1024) + UnifyConstantUtil.MB;
        } else if (size >= 1073741824 && size < 1125899906842624L) {
            sizeStr = String.format(UnifyConstantUtil.UNIT_FORMAT,
                    size / 1024 / 1024 / 1024) + UnifyConstantUtil.GB;
        } else {
            sizeStr = String.format(UnifyConstantUtil.UNIT_FORMAT,
                    size / 1024 / 1024 / 1024 / 1024 / 1024) + UnifyConstantUtil.PB;
        }
        return sizeStr;
    }

    public static void main(String[] args){

        long size = 112589990684262400L;
        System.out.println(adjustUnit(Double.valueOf(size)));

    }
}
