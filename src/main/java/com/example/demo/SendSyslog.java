/*
 * ------------------------------------------------------------------
 * Copyright © 2018 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product:
 *   Module Name:
 *  Date Created: 2021-03-19
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * 2021-03-19     x0334
 * ------------------------------------------------------------------
 */

package com.example.demo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.productivity.java.syslog4j.Syslog;
import org.productivity.java.syslog4j.SyslogIF;
import org.springframework.boot.SpringApplication;

public class SendSyslog {
    public static void main(String[] args) {
        SyslogIF syslog = Syslog.getInstance("udp");
        syslog.getConfig().setHost("127.0.0.1");
        syslog.getConfig().setPort(47321);

        for(int j = 0; j< 10;j++){

            File file = null;
            InputStream in = null;
            String filePath = "D:/honyport.log";
            String info = null;
            String str = "";
            try {
                file = new File(filePath);
                in = new FileInputStream(file);
                byte[] content = new byte[in.available()];
                System.out.println("file bytes: "+content.length);
                InputStreamReader a = new InputStreamReader(in);
                BufferedReader buf = new BufferedReader(a);
                info = buf.readLine();
                str = info;
                System.out.println(str);
                for(int i = 0; i< 100;i++){
                    syslog.info(str);
                }

            } catch (IOException e) {
                System.out.println("error!");
            }


            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

}
