/*
 * ------------------------------------------------------------------
 * Copyright © 2018 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product:
 *   Module Name:
 *  Date Created: 2020-06-12
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * 2020-06-12     x0334
 * ------------------------------------------------------------------
 */

package com.example.demo.guava.map;

import java.util.HashMap;
import java.util.Map;

public class TestMap {
    public static void main(String[] args) throws Exception {
        Map<String, Object> m = new HashMap<>();
        m.put("k", "d");
        System.out.println("k: " + m.get("k"));
        m.put("k", "e");
        System.out.println("k: " + m.get("k"));
    }
}
