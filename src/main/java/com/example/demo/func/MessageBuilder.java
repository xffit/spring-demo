/*
 * ------------------------------------------------------------------
 * Copyright © 2018 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product:
 *   Module Name:
 *  Date Created: 2020-07-22
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * 2020-07-22     x0334
 * ------------------------------------------------------------------
 */

package com.example.demo.func;

@FunctionalInterface
public interface MessageBuilder {
    /**
     * 信息生成器
     * @return 生成的信息
     */
    public abstract String builderMessage();
}
