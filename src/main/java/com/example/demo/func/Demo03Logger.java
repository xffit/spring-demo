/*
 * ------------------------------------------------------------------
 * Copyright © 2018 Hangzhou DtDream Technology Co.,Lt d. All rights reserved.
 * ------------------------------------------------------------------
 *       Product:
 *   Module Name:
 *  Date Created: 2020-07-22
 *   Description:
 * ------------------------------------------------------------------
 * Modification History
 * DATE            Name           Description
 * ------------------------------------------------------------------
 * 2020-07-22     x0334
 * ------------------------------------------------------------------
 */

package com.example.demo.func;

public class Demo03Logger {
    public static void main(String[] args) {
        String msgA = "Hello ";
        String msgB = "World ";
        String msgC = "Java";

        log(1, () -> {
            System.out.println("Lambada 执行！");
            return msgA + msgB + msgC;
        });
    }

    private static void log(int level, MessageBuilder mb) {
        if (level == 1) {
            System.out.println(mb.builderMessage());
        }
    }
}
