package com.example.demo.controller;

import com.example.demo.entity.Student;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @Project demo
 * @Description IStudentService
 * @Author xiaole
 * @Date 2021/8/17 17:31
 */
@RestController
@RequestMapping("/stu/student")
public class StudentController  {

    //修改  先根据id获取结果返回给前端，前端在已有的基础上进行修改
    @GetMapping("/{id}")
    public Student getById(@PathVariable("id") Integer id){
        Student stu = new Student();
        stu.setAge(20);
        stu.setName(String.valueOf(id));
        return stu;
    }
}
