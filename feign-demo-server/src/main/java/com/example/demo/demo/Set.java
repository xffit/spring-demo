package com.example.demo.demo;

import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Project spring-demo
 * @Description Set
 * @Author xiaole
 * @Date 2021/7/10 19:37
 */
public class Set {


    public static void main(String args[]) {

        int percent = 0;
        HashSet<String> idCardSet = new HashSet<String>();
        idCardSet.add("abc");
        idCardSet.add("abcd");
        idCardSet.add("abca");
        idCardSet.add("abcc");
        idCardSet.add("abcdc");
        idCardSet.add("abcdc1");
        idCardSet.add("abcdc2");
        percent = (1 + 1)*100/idCardSet.size();

        String oo = "sacds 我是 曹询县 占 42%， 阿家 共同份额  占 1%";
        int index = oo.indexOf("曹询县");
        if (index == -1) {
            return;
        }
        String sub = oo.substring(index);

        Pattern p = Pattern.compile("\\d+%");
        Matcher m = p.matcher(sub);
        if(m.find()){
            int percent1 = Integer.valueOf(m.group().replace("%",""));
            System.out.println(percent1);
        }
        m = p.matcher(oo.substring(18));
        if(m.find()){
            int percent2 = Integer.valueOf(m.group().replace("%",""));
            System.out.println(percent2);
        }

        System.out.println("住址s".contains("住址"));
        System.out.println("住址".contains("住址"));
        System.out.println("住址".indexOf("住址"));


//        while (m.find()){
//            System.out.println("Match \""+m.group()+"\" at positions "+m.start()+"-"+(m.end()-1));
//        }


    }
}
