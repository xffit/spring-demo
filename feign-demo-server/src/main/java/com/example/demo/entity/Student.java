package com.example.demo.entity;

import lombok.Data;

/**
 * @Project demo
 * @Description Student
 * @Author xiaole
 * @Date 2021/8/17 17:33
 */
@Data
public class Student {
    private String name;
    private Integer age;
}
