package com.example.demo;

import com.example.demo.demo.DemoReq;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	@Test
	void contextTests() {
		ApplicationContext ac = new ClassPathXmlApplicationContext("DemoReq");
		DemoReq demoReq = (DemoReq) ac.getBean("DemoReq");
		System.out.println(demoReq.getName());

	}
}
