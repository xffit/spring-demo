package com.example.demo.entity;

import lombok.Data;

/**
 * @Project feign-demo-client
 * @Description Student
 * @Author xiaole
 * @Date 2021/8/18 16:39
 */
@Data
public class Student {
    private String name;
    private Integer age;
}