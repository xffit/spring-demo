package com.example.demo.client;

import com.example.demo.entity.Student;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


/**
 * @Project feign-demo-client
 * @Description IStudentService
 * @Author xiaole
 * @Date 2021/8/18 16:28
 */
@FeignClient(value = "stu-service-m1")//激活feign客户端，value指定服务名称
public interface IStudentService  {

    //修改  先根据id获取结果返回给前端，前端在已有的基础上进行修改
    @GetMapping("/stu/student/{id}")
    public Student getById(@PathVariable("id") Integer id);
}
