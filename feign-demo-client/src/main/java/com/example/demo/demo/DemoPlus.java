package com.example.demo.demo;

/**
 * @Project spring-demo
 * @Description DemoPlus
 * @Author xiaole
 * @Date 2021/7/30 8:35
 */
public class DemoPlus {

    public static void main(String args[]) {
        int num = 50 ;
        num = num ++ * 2 ;
        System.out.println(num) ;


        int _1num = 2147483647 ;
        _1num += 2L ;
        System.out.println(_1num) ;

        _1num = 2147483647 ;
        _1num += 2 ;
        System.out.println(_1num) ;

        String String = "a";
        System.out.println(String);

    }

}
