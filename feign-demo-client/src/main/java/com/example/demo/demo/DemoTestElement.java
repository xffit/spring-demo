package com.example.demo.demo;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project spring-demo
 * @Description DemoTestElement
 * @Author xiaole
 * @Date 2021/7/12 9:49
 */
public class DemoTestElement {
    public static void main(String args[]){
        List<DemoReq> list = new ArrayList<>();
        DemoReq demoReq = new DemoReq();
        demoReq.setName("kk");
        System.out.println("demoReq origin: " + JSON.toJSONString(demoReq));
        list.add(demoReq);
        demoReq.setList(list);
        System.out.println("demoReq after: " + JSON.toJSONString(demoReq));
    }


}
