package com.example.demo.demo;

import com.example.demo.sub.Page;
import lombok.Data;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @Project spring-demo
 * @Description DemoReq
 * @Author xiaole
 * @Date 2021/7/12 9:30
 */
@Data
public class DemoReq extends Page {
    private Integer pageSize;
    private Integer pageNum;

    private String name;
    public Integer getPageSize() {
        return this.pageSize != null && this.pageSize > 0 ? this.pageSize : super.getPageSize();
    }

    public void setList(List<DemoReq> list) {
        for(DemoReq item: list) {
            item.setName("cdf");
        }
    }
}
