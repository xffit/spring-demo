package com.example.demo.controller;

import com.example.demo.client.IStudentService;
import com.example.demo.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Project feign-demo-client
 * @Description DemoController
 * @Author xiaole
 * @Date 2021/8/18 16:22
 */
@RestController
@RequestMapping("/v1/api")
public class DemoController {

    @Autowired
    private IStudentService studentService;

    @GetMapping("/demo1")
    public String demo1(){
        Student stu = studentService.getById(3);
        return String.valueOf(stu.getName());
    }

}