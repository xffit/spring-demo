package com.example.demo.sub;

/**
 * @Project spring-demo
 * @Description Page
 * @Author xiaole
 * @Date 2021/7/12 9:29
 */

public class Page {
    /**
     * 每页条数
     */
    private Integer pageSize;

    /**
     * 当前页数
     */
    private Integer pageNum;

    public Integer getPageNum() {
        return this.pageNum != null && this.pageNum >= 0 ? this.pageNum : 1;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return this.pageSize != null && this.pageSize > 0 ? this.pageSize : 10;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
