package com.example.demo.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.extern.slf4j.Slf4j;

/**
 * @Project spring-demo
 * @Description SchedulerTask
 * @Author xiaole
 * @Date 2021/5/14 10:35
 */

@Slf4j
@Component
public class SchedulerTask {
    Logger log = LoggerFactory.getLogger(SchedulerTask.class);

    //cron表达式：每隔5秒执行一次
    @Scheduled(cron = "0 0 0/5 * * *")
    public void scheduled(){
        log.info("使用cron: {}");
        System.out.println(new BCryptPasswordEncoder().encode("yx525@nacos"));

    }
    //上一次 启动时间点之后每50秒执行一次
            //如果任务时长超过 fixedRate不会启动多个任务实例，只不过会在上次任务执行完后立即启动下一轮
            //除非这个类或 Job 方法用 @Async 注解了，使得任务不在 TaskScheduler 线程池中执行，而是每次创建新线程来执行。
    @Scheduled(fixedRate = 500000)
    public void scheduled1() {
        log.info("使用fixedRate {}");
    }
    //上一次 结束时间点之后 每50秒执行一次
    @Scheduled(fixedDelay = 500000)
    public void scheduled2() {
        log.info("使用fixedDelay {}");
    }
    //第一次延迟 10秒执行，之后按照fixedRate的规则每60秒执行
    @Scheduled(initialDelay = 10000,fixedRate = 600000)
    public void scheduled3() {
        log.info("使用initialDelay {}");
    }


}
